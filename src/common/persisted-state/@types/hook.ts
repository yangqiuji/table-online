import {Dispatch, SetStateAction} from "react";

export type persistedState<T> = [T , Dispatch<SetStateAction<T>> ]

export type usePersistedState = <T>(initialValue: T | (() => T)) => persistedState<T>
