import {default as createSyncPersistedState} from './create-persisted-state'
import createAsyncPersistedState from './create-async-persisted-state'

import isAsyncStorage from './utils/is-async-storage'

import {AsyncStorage, Storage} from './@types/storage'
import {usePersistedState} from './@types/hook'
import storage from "@/common/persisted-state/storages/local-storage";
import defaultStorage from "@/common/persisted-state/storages/default-storage";
import {useState} from "react";

export function createPersistedState(
  name: string,
  storage: Storage | AsyncStorage = defaultStorage,
): usePersistedState {
  if (storage) {
    if (isAsyncStorage(storage))
      return useState
    // return createAsyncPersistedState(name, storage)
    else
      return createSyncPersistedState(name, storage)
  }
  return useState;

}

const storageKey = 'docs-theme';

export default createPersistedState(storageKey, storage)


