import React, {useEffect} from 'react'
import {AsyncStorage, Storage, StorageChange} from '../@types/storage'
import isFunction from './is-function'

function getValue<T>(key: string, value: string) {
  let newState = null

  try {
    newState = JSON.parse(value)
  } catch (err) {
    console.error('use-persisted-state: Can\'t parse value from storage', err)
  }

  return newState && key in newState ? newState[key] as T : null
}

export default function useStorageHandler<T>(
  storageKey: string,
  setState: React.Dispatch<React.SetStateAction<T>>,
) {
  return (changes: { [key: string]: StorageChange }): void => {
    Object.entries(changes).forEach(([key, change]) => {
      if (
        key === storageKey && !!change.newValue && change.oldValue != change.newValue) {
        setState(change.newValue)
      }
    })
  }
}
