import isFunction from './is-function'

export default function <T>(initialValue: T | (() => T), persist?: string): T {
  let initialPersist: T = '' as T
  try {
    initialPersist = persist ? JSON.parse(persist) : '' as T
  } catch (ignore) {
    initialPersist= persist?persist:'' as T
  } // eslint-disable-line no-empty
  if (!!initialPersist)
    return initialPersist

  return isFunction(initialValue) ? initialValue() : initialValue
}
