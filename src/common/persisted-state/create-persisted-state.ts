import React, {useEffect, useState} from 'react'

import {Storage} from './@types/storage'
import {persistedState, usePersistedState} from './@types/hook'

import useStorageHandler from './utils/use-storage-handler'
import getNewValue from './utils/get-new-value'
import getPersistedValue from './utils/get-persisted-value'
import {listeners, useEventListener} from "@/common/persisted-state/utils/create-web-storage";


export default function createPersistedState(
  storageKey: string,
  storage: Storage,
): usePersistedState {
  const safeStorageKey = `ibr-${storageKey}`;
  const usePersistedState = <T>(initialValue: T | (() => T)): persistedState<T> => {
    const persist = storage.get(safeStorageKey)
    const persistedState = persist[safeStorageKey]
    const initialOrPersistedValue = getPersistedValue<T>(initialValue, persistedState)
    const [state, setState] = useState<T>(initialOrPersistedValue)

    useEventListener(safeStorageKey)

    const setPersistedState = (newState: React.SetStateAction<T>): void => {
      const newValue = getNewValue<T>(newState, state)
      storage.set({[safeStorageKey]: newValue})
    };

    useEffect(() => {
      const handleStorage = useStorageHandler<T>(safeStorageKey, setState)
      storage.onChanged.addListener(handleStorage)
      return () => {
        if (storage.onChanged.hasListener(handleStorage)) {
          storage.onChanged.removeListener(handleStorage)
        }
      }
    }, [initialValue])

    return [state, setPersistedState]
  }
  return usePersistedState;
}
