const NAMESPACE=process.env.IBOOM_NAMESPACE|| "ibr";

export const CIRCLE_BUTTON=`${NAMESPACE}-circle-button`;

export const TOPBAR=`${NAMESPACE}-top-bar`;
export const TOPBAR_SHUT=`${TOPBAR}-shut`;
export const TOPBAR_TITLE=`${TOPBAR}-title`;
export const TOPBAR_TITLE_POP=`${TOPBAR_TITLE}-pop`;
export const TOPBAR_TITLE_BORDER=`${TOPBAR}-border`;
export const TOPBAR_LOGO=`${TOPBAR}-logo`;
export const TOPBAR_FEATURES=`${TOPBAR}-features`;
export const TOPBAR_SETTING=`${TOPBAR}-setting`;



