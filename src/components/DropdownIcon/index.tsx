import { Popover2 } from '@blueprintjs/popover2';
import { Icon, Popover } from '@blueprintjs/core';
import PopmenuOnTableTitle from '@/pages/workbench/PopmenuOnTableTitle';
import React from 'react';

const DropdownIcon = () => {
  return (
    <Popover2>
      <Icon
        icon="caret-down"
        iconSize={14}
        className="ibr-content-table-top-bar-title-menu"
      ></Icon>
      <PopmenuOnTableTitle />
    </Popover2>
  );
};
