import React from 'react';
import { Classes } from '@/common';
import classNames from 'classnames';
import Props from '@/common/props';

const CircleButton: React.FC<CircleButtonProps> = props => {
  const { className, size = 16, color, stroke, icon } = props;
  const classes = classNames(Classes.CIRCLE_BUTTON, className);
  const viewBox = `0 0 16 16`;

  return (
    <svg width={size} height={size} viewBox={viewBox} className={classes}>
      <circle cx={8} cy={8} r={8} fill={color} stroke={stroke}></circle>
      <image
        xlinkHref={icon}
        width={12}
        height={12}
        x={2}
        y={2}
        opacity={0}
      ></image>
    </svg>
  );
};

interface CircleButtonProps extends Props {
  size?: number;
  color: string;
  stroke?: string;
  icon: string;
}

export default CircleButton;
