import * as React from "react";
import {useEffect, useState} from "react";
import ThemeContext, {theme, themeDark, themeLight, themeVaule} from "./ThemeContext";
import {hump2Dash} from "@/common/util";
import usePersistedState from "@/common/persisted-state"

interface ThemeProps {

}

const ThemeProvider: React.FC<ThemeProps> = (props) => {
  const {children} = props;
  const [themeLocal, setThemLocal] = usePersistedState<string>("ibr-dark");
  let theme: theme = themeLocal === "ibr-dark" ? themeDark : themeLight;

  const setCSSVariables = (theme: theme): void => {
    for (const key in theme) {
      document.documentElement.style.setProperty(`--${hump2Dash(key)}`, theme[key]);
      // console.log(docum.style.getPropertyPriority(`--${key}`));
    }
  };

  const toggleTheme = () => {
    if (themeLocal === "ibr-dark") {
      theme = themeLight;
      setThemLocal("ibr-light");
    } else {
      theme = themeDark;
      setThemLocal("ibr-dark");
    }
  };

  useEffect(() => {
    setCSSVariables(theme);
  }, [themeLocal]);

  const value: themeVaule = {toggle: toggleTheme, theme: theme};

  return (<ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>);
};

export default ThemeProvider;
