import React from 'react';

export interface theme {
  [index: string]: string;

  //bg
  colorBgMainWindows: string;
  colorBgTopBar: string;
  colorBgContentSider: string;
  colorBgContent: string;
  colorBgTopTitlePopPanel: string;
  colorBgSwitchButton: string;
  colorBgTabNode: string;
  //text
  colorTextTopTitle: string;
  colorTextSearch: string;
  colorText: string;
  colorTextBlack: string;
  //border
  colorPopPanelBorder: string;
  colorSiderDivider: string;
  //colour
  colorBule: string;
  //button
  colorButtonInfo: string;
  colorButtonInfoHover: string;
  colorButtonMenu: string;

  BrandingBlue: string;
  BrandingOrange: string;
  colorOrg: string;
  colorRed: string;
  Cyan: string;
  TertiaryBackgroundColor: string;
  Gray: string;
  SecondaryTextColor: string;
  TertiaryTextColor: string;
}

export const themeDark: theme = {
  //background-color
  colorBgMainWindows: '#182026',
  colorBgTopBar: '#293742',
  colorBgTopTitlePopPanel: '#30404D',
  colorBgContentSider: '#30404D',
  colorBgContent: '#202B33',
  colorBgSwitchButton: '#202B33',
  colorBgTabNode: '#E1E8ED',

  //Text color
  colorTextTopTitle: '#D8E1E8',
  colorText: '#E1E8ED',
  colorTextSearch: '#D8E1E8',
  colorTextBlack: '#FFFFFF',
  //border
  colorPopPanelBorder: '#394B59',
  colorSiderDivider: '#E1E8ED',
  colorBule: '#287DFA',
  //button
  colorButtonInfo: '#CED9E0',
  colorButtonInfoHover: '#BFCCD6',
  colorButtonMenu: '#EBF1F5',

  BrandingBlue: '#287DFA',
  BrandingOrange: '#FFB400',
  Cyan: '#06AEBD',
  Gray: '#ACB4BF',
  colorOrg: '#FF6F00',
  colorRed: '#EE3B28',
  SecondaryTextColor: '#D8E1E8',
  TertiaryBackgroundColor: '#5C7080',
  TertiaryTextColor: '#A7B6C2',
};

export const themeLight: theme = {
  //background-color
  colorBgMainWindows: '#F5F8FA',
  colorBgTopBar: '#E1E8ED',
  colorBgTopTitlePopPanel: '#D8E1E8',
  colorBgContentSider: '#D8E1E8',
  colorBgContent: '#EBF1F5',
  colorBgSwitchButton: '#E1E8ED',
  colorBgTabNode: '#CED9E0',
  //Text color
  colorTextTopTitle: '#182026',
  colorText: '#394B59',
  colorTextSearch: '#5C7080',
  colorTextBlack: '#10161A',
  //border
  colorPopPanelBorder: '#BFCCD6',
  colorSiderDivider: '#5C7080',
  colorBule: '#7EB0FC',

  //button
  colorButtonInfo: '#A7B6C2',
  colorButtonInfoHover: '#5C7080',
  colorButtonMenu: '#293742',

  BrandingBlue: '#7EB0FC',
  BrandingOrange: '#FFD266',
  Cyan: '#50C6D0',
  Gray: '#5C697C',
  colorOrg: '#FFA866',
  colorRed: '#F37668',
  SecondaryTextColor: '#30404D',
  TertiaryBackgroundColor: '#CED9E0',
  TertiaryTextColor: '#8A9BA8',
};

export default React.createContext({
  theme: themeDark,
  toggle: () => {},
});

export interface themeVaule {
  theme: theme;
  toggle: () => void;
}
