import React, { useState } from 'react';
import _ from 'lodash';
import SiderSwitchButton from '@/components/ResizeBox/SiderSwitchButton';
import Props from '@/common/props';
import classNames from 'classnames';

const ResizeBox: React.FC<ResizePorps> = (props: ResizePorps) => {
  const {
    adjustLimitWidth = 0,
    adjustMaxWidth = 900,
    leftElement,
    rightElement,
    initialWidth = 280,
    titleDisplay,
    siderActive = true,
    className,
  } = props;
  const [adjustWidth, SetadjustWidth] = useState(initialWidth);
  const [active, SetActive] = useState(siderActive);
  const [isCover, SetCaver] = useState(false);

  const handleStopResize = () => {
    SetCaver(false);
  };

  const resizeThrottle = _.throttle((widthNUm: number) => {
    if (widthNUm >= adjustLimitWidth && widthNUm <= adjustMaxWidth) {
      SetadjustWidth(widthNUm);
    }
  }, 100);

  const handleResizeMove = (event: React.MouseEvent) => {
    let width = event.nativeEvent.offsetX;
    if (resizeThrottle) (resizeThrottle as Function)(width);
  };

  const handleResizeMouseDown = () => {
    SetCaver(true);
  };

  const handleSiderActiveOnClick = () => {
    let isActive: boolean;
    if (active) {
      isActive = false;
    } else {
      isActive = true;
    }
    SetActive(isActive);
  };

  const newClassName = classNames('main-content', className);

  return (
    <div
      className={newClassName}
      onMouseUp={handleStopResize}
      onMouseLeave={handleStopResize}
    >
      <div
        className="ibr-content-left-navbar-title"
        style={titleDisplay ? { display: 'inline' } : { display: 'none' }}
      >
        <SiderSwitchButton
          onClick={handleSiderActiveOnClick}
          active={active}
        ></SiderSwitchButton>
      </div>
      {React.cloneElement(leftElement, { width: adjustWidth, display: active })}
      {/*<ResizeLeft wigth={adjustWidth}>*/}
      {/*<h1>左边导航栏</h1>*/}
      {/*</ResizeLeft>*/}
      <div
        draggable={false}
        onMouseDown={handleResizeMouseDown}
        className="ibr-content-sider-resize-divider"
        style={active ? { opacity: 1 } : { opacity: 0 }}
      />
      {rightElement}
      {isCover && (
        <div
          className="ibr-content-sider-resize-cover"
          onMouseMove={handleResizeMove}
        ></div>
      )}
    </div>
  );
};

interface ResizePorps extends Props {
  adjustLimitWidth?: number;
  adjustMaxWidth?: number;
  initialWidth?: number;
  titleDisplay?: boolean | undefined;
  siderActive?: boolean | undefined;
  leftElement: JSX.Element;
  rightElement: JSX.Element;
}

export default ResizeBox;
