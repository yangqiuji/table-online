import React from 'react';
import classNames from 'classnames';
import Props from '@/common/props';

const ResizeLeft: React.FC<ResizeLeftPorps> = props => {
  const { children, width, className, display } = props;
  const newClassName = classNames('ibr-content-sider', className);

  const styles = display
    ? { width: width, opacity: 1 }
    : { width: width, opacity: 0, display: 'none' };

  return (
    <div className={newClassName} style={styles}>
      {children}
    </div>
  );
};

interface ResizeLeftPorps extends Props {
  width?: number;
  display?: boolean;
}

export default ResizeLeft;
