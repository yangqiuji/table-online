import React from 'react';
import Props from '@/common/props';
import classNames from 'classnames';

const ResizeRight: React.FC<ResizeRightPorps> = props => {
  const { children, className } = props;
  const classes = classNames('ibr-content-main-content', className);
  return <div className={classes}>{children}</div>;
};

interface ResizeRightPorps extends Props {}

export default ResizeRight;
