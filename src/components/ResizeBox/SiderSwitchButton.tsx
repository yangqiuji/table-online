import retract from "@/assets/retract.svg";
import unfold from "@/assets/unfold.svg";
import React from "react";
import classNames from "classnames";

const SiderSwitchButton: React.FC<SiderSwitchButtonProps> = (props) => {
  const {onClick, active} = props;

  const className = classNames('ibr-content-left-navbar-switch-button', active ? 'ibr-button-select' : 'ibr-button-unselect')
  const icon = active ? retract : unfold;

  return (<button onClick={onClick} className={className}>
    <img src={icon}></img>
    <p>项目目录</p>
  </button>);
};

interface SiderSwitchButtonProps {
  onClick: ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void) | undefined;
  active: boolean;
}

export default SiderSwitchButton;
