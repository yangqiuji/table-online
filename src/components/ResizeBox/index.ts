import ResizeBox from '@/components/ResizeBox/Resizebox';
import SiderSwitchButton from '@/components/ResizeBox/SiderSwitchButton';
import ResizeLeft from '@/components/ResizeBox/ResizeLeft';
import ResizeRight from '@/components/ResizeBox/ResizeRight';

export { ResizeBox, ResizeLeft, ResizeRight, SiderSwitchButton };
