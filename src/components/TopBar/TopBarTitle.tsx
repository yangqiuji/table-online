import {connect, Link} from "umi";
import React from "react";
import classNames from "classnames";
import {Classes} from "@/common";
import {Icon, Label, Popover, Position, Switch} from "@blueprintjs/core";
import WorkSpacePopPanel from "@/components/TopBar/WorkSpacePopPanel";
import Props from "@/common/props";


const TopBarTitle: React.FC<Props> = (props) => {
  const {className} = props;
  const TitleClasses = classNames(Classes.TOPBAR_TITLE, className);

  const modifiers = {
    offset: {offset: '0,-1'},
  };

  return (
    <Popover className={TitleClasses} minimal={true} boundary="window" position={Position.BOTTOM}
             modifiers={modifiers} popoverClassName='ibr-top-bar-border'>
      <Label style={{marginBottom: 0}}><Icon icon='applications' iconSize={15}
                                             style={{paddingBottom: 2, paddingRight: 6}}/>项目交付文档<Icon
        icon='chevron-down' iconSize={15} style={{paddingBottom: 2, paddingLeft: 1}}/></Label>
      <WorkSpacePopPanel placeholder="项目交付文档"></WorkSpacePopPanel>
    </Popover>
  );
};

export default connect()(TopBarTitle)
