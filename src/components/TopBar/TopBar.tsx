import classNames from "classnames";
import {Classes} from '@/common';
import React, {useContext, useState} from "react";
import {Link} from "umi";
import closeIcon from "@/assets/close.svg";
import fullScreenIcon from "@/assets/fullscreen.svg";
import {Button, ButtonGroup, Icon, IconName, MaybeElement, Tooltip} from "@blueprintjs/core";
import logoImg from "@/assets/table-log.svg";
import {history} from "@@/core/history";
import TopBarTitle from "@/components/TopBar/TopBarTitle";
import TopBarIconButtom from "@/components/TopBar/TopBarIconButton";
import CircleButton from "@/components/CircleButton/CircleButton";
import ThemeContext, {themeVaule} from "@/components/ThemeSelector/ThemeContext";
import Props from "@/common/props";

const TopBar: React.FC<TopBarProps> = (props) => {
  const {className} = props;
  const TopBarClasses = classNames(Classes.TOPBAR, className);
  const [activeItem, setActiveItem] = useState('workbench');

  const handleChick = ({name, helf}: { name: string, helf: string }): void => {
    if (name != undefined) {
      setActiveItem(name);
    }
    if (history && helf != undefined)
      history.push(helf);
  };

  const ferturesMenus: menuProps[] = [
    {icon: 'desktop', content: '工作台', iconSiz: 20, name: 'workbench', helf: '/workbench'},
    {icon: 'projects', content: '工作空间', iconSiz: 20, name: 'workspace', helf: '/workspace'},
    {icon: 'inherited-group', content: '组织权限', iconSiz: 20, name: 'roles', helf: '/workbench'},
    {icon: 'search-template', content: '模版', iconSiz: 20, name: 'templates', helf: '/workbench'}];

  const settingMenus: menuProps[] = [
    {icon: 'help', iconSiz: 20, name: 'help', helf: '/workbench'},
    {icon: 'envelope', content: '消息中心', iconSiz: 20, name: 'message', helf: '/workbench'},
    {icon: 'user', content: '个人中心', iconSiz: 20, name: 'personal center', helf: '/workbench'},
  ];

  const dynamicMenus = (menus: menuProps[]) => {
    const buttons = menus.map((prop, index) => {
      return (
        <Tooltip key={index} content={prop.content} position="bottom">
          <Button active={activeItem === prop.name}
                  onClick={() => {
                    handleChick({name: prop.name, helf: prop.helf})
                  }}>
            <Icon icon={prop.icon} iconSize={prop.iconSiz}></Icon></Button>
        </Tooltip>)
    });
    return (<ButtonGroup minimal className='top-bar-features-buttons'>
      {buttons}
    </ButtonGroup>)
  };

  const {theme}: themeVaule = useContext(ThemeContext);

  return (<div className={TopBarClasses}>
      <div className={Classes.TOPBAR_SHUT}>
        <CircleButton icon={closeIcon} size={13} color={theme.colorRed}></CircleButton>
        <CircleButton icon={fullScreenIcon} size={13} color={theme.colorOrg}></CircleButton>
      </div>
      <TopBarTitle></TopBarTitle>
      <Link to='' className={Classes.TOPBAR_LOGO}><img src={logoImg} style={{width: 35, height: 35}}></img></Link>
      <TopBarIconButtom className={Classes.TOPBAR_FEATURES}>
        {dynamicMenus(ferturesMenus)}
      </TopBarIconButtom>
      <TopBarIconButtom className={Classes.TOPBAR_SETTING}>
        {dynamicMenus(ferturesMenus)}
      </TopBarIconButtom>
    </div>
  );
};

interface TopBarProps extends Props {
}

interface menuProps {
  icon: IconName | MaybeElement;
  content?: JSX.Element | string;
  iconSiz: number;
  name: string;
  helf: string;
}

export default TopBar;
