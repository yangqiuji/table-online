import * as React from 'react';
import { useContext } from 'react';
import {
  EditableText,
  Menu,
  MenuDivider,
  MenuItem,
  Switch,
} from '@blueprintjs/core';
import classNames from 'classnames';
import { Classes } from '@/common';
import Props from '@/common/props';
import ThemeContext, {
  themeVaule,
} from '@/components/ThemeSelector/ThemeContext';

export default (props: WorkSpacePopPanelProps) => {
  const { className, placeholder } = props;
  const PopClasses = classNames(Classes.TOPBAR_TITLE_POP, className);
  const { toggle }: themeVaule = useContext(ThemeContext);

  return (
    <div className={PopClasses}>
      <EditableText
        minWidth={265}
        defaultValue={placeholder}
        selectAllOnFocus={true}
        alwaysRenderInput={true}
        className="ibr-top-bar-title-pop-panel-edit-text"
        isEditing={false}
      ></EditableText>
      <Menu className="ibr-top-bar-title-pop-panel-menu">
        <MenuItem text="项目1" htmlTitle="yangqiuji"></MenuItem>
        <MenuItem text="项目2"></MenuItem>
        <MenuItem text="项目3"></MenuItem>
        <MenuItem text="项目4"></MenuItem>
        <MenuDivider />
        <MenuItem icon="share" text="分享工作空间"></MenuItem>
        <MenuItem icon="trash" text="删除工作空间"></MenuItem>
        <MenuItem icon="switch" text="管理工作空间"></MenuItem>
      </Menu>
      <Switch
        alignIndicator="right"
        label="黑暗模式"
        innerLabel="on"
        innerLabelChecked="off"
        onChange={toggle}
      ></Switch>
    </div>
  );
};

interface WorkSpacePopPanelProps extends Props {
  placeholder: string;
}
