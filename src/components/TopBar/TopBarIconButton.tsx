import React from "react";
import {ButtonGroup} from "@blueprintjs/core";
import classNames from "classnames";
import Props from "@/common/props";

const TopBarIconButtom: React.FC<Props> = (props) => {
  const {className, children} = props;
  const Classes = classNames(className);
  return (<ButtonGroup className={Classes}>{children}</ButtonGroup>)
};


export default TopBarIconButtom;
