import { Button, ButtonGroup } from '@blueprintjs/core';
import { Cell, Column, Table } from '@blueprintjs/table';
import classNames from 'classnames';
import React from 'react';

const GridView = props => {
  const buttonsStyle = {};

  const cellRenderer = (rowIndex: number) => {
    if (rowIndex == 4) return <Cell>+</Cell>;
    else return <Cell>{`$${(rowIndex * 10).toFixed(2)}`}</Cell>;
  };

  return (
    <div
      className={classNames(
        'ibr-scroll-tabs-pane-views',
        'ibr-scroll-tabs-pane-views-grid',
      )}
    >
      <ButtonGroup minimal className="ibr-scroll-tabs-pane-views-grid-buttons">
        <Button icon="add" text="新建记录" style={buttonsStyle}></Button>
        <Button icon="eye-off" text="隐藏字段"></Button>
        <Button icon="th-filtered" text="筛选"></Button>
        <Button icon="multi-select" text="分组"></Button>
        <Button icon="sort" text="排序"></Button>
        <Button icon="bring-data"></Button>
        <Button icon="document-share" text="分享"></Button>
      </ButtonGroup>
      <div className="ibr-scroll-tabs-pane-views-grid-table">
        <Table
          numRows={5}
          enableGhostCells={false}
          enableRowHeader={true}
          enableRowReordering={true}
          numFrozenColumns={1}
        >
          <Column name="name" cellRenderer={cellRenderer}></Column>
          <Column name="+"></Column>
        </Table>
      </div>
    </div>
  );
};

export default GridView;
