import { ReactElement, Ref, useState } from 'react';
import { TabNodeProps } from 'rc-tabs/es/TabNavList/TabNode';
import * as React from 'react';
import classNames from 'classnames';
import KeyCode from 'rc-util/es/KeyCode';
import './css/scrollTabs.css';
import TabNodeTitle from '@/components/ScrollTabs/TabNodeTitle';
import { DraggableProvided } from 'react-beautiful-dnd';

interface TabNodeExProps extends TabNodeProps {
  provided: DraggableProvided;
}

const TabNode = (
  props: TabNodeExProps,
  ref: Ref<HTMLDivElement>,
): ReactElement | null => {
  const {
    tab,
    editable,
    closable,
    prefixCls,
    active,
    onClick,
    id,
    onFocus,
    provided,
  } = props;
  const { key, disabled } = tab;
  const removable = editable && closable !== false && !disabled;
  const tabPrefix = ''.concat(prefixCls, '-scroll-tabs', '-tab');
  const classes = classNames(tabPrefix, {
    [''.concat(tabPrefix, '-active')]: active,
    [''.concat(tabPrefix, '-disabled')]: disabled,
  });

  const onInternalClick = (e: React.MouseEvent | React.KeyboardEvent) => {
    if (disabled) return;
    e.stopPropagation();
    if (onClick) onClick(e);
  };

  const onRemoveTab = (event: React.MouseEvent | React.KeyboardEvent) => {
    event.preventDefault();
    event.stopPropagation();
    if (editable)
      editable.onEdit('remove', {
        key: key,
        event: event,
      });
  };

  const [isEditing, setIsEditing] = useState(false);

  const handleCellDoubleClick = (_event: MouseEvent) => {
    setIsEditing(true);
  };

  const handleCellActivate = (_event: MouseEvent) => {
    return true;
  };

  const handleKeyDown = e => {
    if (['Enter', ' '].includes(e.key)) {
      e.preventDefault();
      onInternalClick(e);
    }
    if (isEditing) {
      return;
    }
    // setting dirty value to empty string because apparently the text field will pick up the key and write it in there
    setIsEditing(true);
  };

  return (
    <div
      key={key}
      ref={ref}
      className={classes}
      aria-selected={active}
      id={id && ''.concat(id, '-tab-').concat(key)}
      aria-controls={id && ''.concat(id, '-panel-').concat(key)}
      aria-disabled={disabled}
      role="tab"
      tabIndex={disabled ? void 0 : 0}
      onClick={onInternalClick}
      onKeyDown={handleKeyDown}
      onDoubleClick={handleCellDoubleClick}
      onActivate={handleCellActivate}
      onFocus={onFocus}
      {...provided.dragHandleProps}
      {...provided.draggableProps}
    >
      {/*<TabNodeTitle isEditing={isEditing} value={tab.tab} onChangeEditMode={handleCellDoubleClick}/>*/}
      {tab.tab}
    </div>
  );
};

export default React.forwardRef(TabNode);
