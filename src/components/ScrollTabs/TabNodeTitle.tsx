import React, { ReactElement, useEffect, useRef, useState } from 'react';
import {
  EditableText,
  IEditableTextProps,
  IIntentProps,
} from '@blueprintjs/core';
import Props from '@/common/props';
import classNames from 'classnames';
import * as Classes from '@blueprintjs/table/src/common/classes';
import { Draggable } from '@blueprintjs/table/src/interactions/draggable';
import { Classes as CoreClasses } from '@blueprintjs/core/lib/esm/common';

interface TabNodeTitleProps extends IEditableTextProps {
  interactive?: boolean;
  isFocused?: boolean;
  loading?: boolean;
  onChangeEditMode: (value: boolean) => void;
}

const TabNodeTitle = (props: TabNodeTitleProps): ReactElement | null => {
  const {
    className,
    isEditing,
    value,
    interactive,
    intent,
    onCancel,
    onChange,
    onConfirm,
    truncated,
    wrapText,
    isFocused,
    loading,
    onChangeEditMode,
  } = props;

  const [dirtyValue, setDirtyValue] = useState<string | undefined>();
  const [savedValue, setSavedValue] = useState<string | undefined>(value);

  const handleCancel = (value: string) => {
    // don't strictly need to clear the dirtyValue, but it's better hygiene
    onChangeEditMode(false);
    setDirtyValue(savedValue);
    if (onCancel) onCancel(value);
  };

  const handleEdit = () => {
    onChangeEditMode(true);
    setDirtyValue(savedValue);
  };

  const handleChange = (value: string) => {
    if (onChange) onChange(value);
    setDirtyValue(value);
  };

  const handleConfirm = (value: string) => {
    onChangeEditMode(false);
    setDirtyValue(savedValue);
    setSavedValue(value);
    if (onConfirm) onConfirm(value);
  };

  const classes = classNames(
    CoreClasses.intentClass(intent),
    {
      [Classes.TABLE_CELL_INTERACTIVE]: interactive,
      [CoreClasses.LOADING]: loading,
      [Classes.TABLE_TRUNCATED_CELL]: truncated,
    },
    className,
  );

  const divRef = useRef<HTMLDivElement>();

  useEffect(() => {
    if (isFocused && !isEditing) {
      // don't focus if we're editing -- we'll lose the fact that we're editing
      divRef.current.focus();
    }
  });

  if (isEditing) {
    const className = className ? className : null;
    return (
      <EditableText
        isEditing={false}
        className={classNames(
          Classes.TABLE_EDITABLE_TEXT,
          Classes.TABLE_EDITABLE_NAME,
          className,
        )}
        intent={intent}
        minWidth={null}
        onCancel={handleCancel}
        onChange={handleChange}
        onConfirm={handleConfirm}
        onEdit={handleEdit}
        placeholder=""
        selectAllOnFocus={false}
        value={dirtyValue}
      />
    );
  } else {
    const textClasses = classNames('ibr-tab-navlist-editable-text', {
      [Classes.TABLE_TRUNCATED_TEXT]: truncated,
      [Classes.TABLE_NO_WRAP_TEXT]: !wrapText,
    });

    return <div className={textClasses}>{savedValue}</div>;
  }
};

export default TabNodeTitle;
