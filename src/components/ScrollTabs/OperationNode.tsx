import React, { LegacyRef, ReactElement } from 'react';
import { Popover2 } from '@blueprintjs/popover2';
import { OperationNodeProps } from 'rc-tabs/es/TabNavList/OperationNode';
import { Icon } from '@blueprintjs/core/lib/cjs';

const OperationNode = (
  props: OperationNodeProps,
  ref: LegacyRef<HTMLDivElement>,
): ReactElement | null => {
  return (
    <Popover2>
      <Icon
        icon="menu"
        className="ibr-scroll-tabs-nav-operation"
        iconSize={14}
      />
    </Popover2>
  );
};

export default React.forwardRef(OperationNode);
