import React, { ReactNode, useContext } from 'react';
import Props from '@/common/props';
import { Tabs } from 'antd';
import { rederBar } from '@/components/ScrollTabs/TabNavList';
import { TabsProps } from 'rc-tabs/es';
import ThemeContext from '@/components/ThemeSelector/ThemeContext';
import TabNodeTitle from '@/components/ScrollTabs/TabNodeTitle';
import { TabPaneProps } from 'rc-tabs';

const { TabPane } = Tabs;

export interface tabPans {
  key: string;
  value: string;
  paneContent: JSX.Element;
  className?: string;
  style?: React.CSSProperties;
  paneProps: TabPaneProps;
}

interface ScrollTabsProps extends Props, TabsProps {
  panes: tabPans[];
}

const ScrollTabs: React.FC<ScrollTabsProps> = props => {
  const { tabBarExtraContent, panes } = props;
  const themes = useContext(ThemeContext);

  const intterStyle = {
    flex: 'auto',
    color: themes.theme.colorTextBlack,
  };

  const topbarStyle = {
    backgroundColor: themes.theme.colorBgTopBar,
    margin: '0 0 6px 0',
    borderBottom: '1px solid themes.theme.colorBgContentSider',
  };

  const tabPanes = panes.map((pane, index) => (
    <TabPane
      tab={<TabNodeTitle value={pane.value}></TabNodeTitle>}
      {...pane.paneProps}
      key={pane.key}
    >
      {pane.paneContent}
    </TabPane>
  ));

  return (
    <Tabs
      defaultActiveKey="1"
      tabPosition="top"
      style={intterStyle}
      renderTabBar={rederBar}
      type="editable-card"
      tabBarExtraContent={tabBarExtraContent}
      tabBarStyle={topbarStyle}
    >
      {tabPanes}
    </Tabs>
  );
};

export default ScrollTabs;
