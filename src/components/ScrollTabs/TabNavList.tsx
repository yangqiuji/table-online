import * as React from 'react';
import {
  LegacyRef,
  ReactElement,
  RefObject,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import TabContext from 'rc-tabs/es/TabContext';
import useSyncState from 'rc-tabs/es/hooks/useSyncState';
import useOffsets from 'rc-tabs/es/hooks/useOffsets';
import useTouchMove from 'rc-tabs/es/hooks/useTouchMove';
import useVisibleRange from 'rc-tabs/es/hooks/useVisibleRange';
import useRaf from 'rc-tabs/es/hooks/useRaf';
import classNames from 'classnames';
import AddButton from 'rc-tabs/es/TabNavList/AddButton';
import { Tab, TabBarExtraContent, TabBarExtraMap } from 'rc-tabs/es/interface';
import { TabNavListProps } from 'rc-tabs/es/TabNavList';
import TabNode from '@/components/ScrollTabs/TabNode';
import OperationNode from '@/components/ScrollTabs/OperationNode';
import ResizeObserver from 'rc-resize-observer';
import './css/scrollTabs.css';
import {
  DragDropContext,
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
  Droppable,
  DroppableProvided,
  DroppableStateSnapshot,
  DropResult,
} from 'react-beautiful-dnd';
import useRefs from 'rc-tabs/es/hooks/useRefs';

interface ExtraContentProps {
  position: string;
  prefixCls: string;
  extra: TabBarExtraContent | null | undefined;
}

const ExtraContent = (props: ExtraContentProps) => {
  const { position, prefixCls, extra } = props;
  if (!extra) return null;
  else {
    let content;
    const assertExtra = extra;

    if (position === 'right') {
      if ((assertExtra as TabBarExtraMap).right) {
        content = (assertExtra as TabBarExtraMap).right;
      } else if (!(assertExtra as TabBarExtraMap).left) content = assertExtra;
      else content = null;
    }

    if (position === 'left') {
      if ((assertExtra as TabBarExtraMap).left) {
        content = (assertExtra as TabBarExtraMap).left;
      } else content = null;
    }

    return content
      ? React.createElement(
          'div',
          {
            className: ''.concat(prefixCls, '-extra-content'),
          },
          content,
        )
      : null;
  }
};

const TabNavList = (
  props: TabNavListProps,
  ref: LegacyRef<HTMLDivElement>,
): ReactElement | null => {
  let { prefixCls, tabs } = useContext(TabContext);

  const {
    className,
    style,
    id,
    animated,
    activeKey,
    rtl,
    extra,
    editable,
    locale,
    tabPosition,
    tabBarGutter,
    children,
    onTabClick,
    onTabScroll,
  } = props;
  const tabsWrapperRef = useRef<HTMLDivElement | null>();
  const tabListRef = useRef<HTMLDivElement>();
  const operationsRef = useRef<HTMLDivElement>();
  const innerAddButtonRef = useRef<HTMLButtonElement>();
  const [getBtnRef, removeBtnRef] = useRefs<HTMLDivElement | null>();

  const tabPositionTopOrBottom =
    tabPosition === 'top' || tabPosition === 'bottom';
  const [transformLeft, setTransformLeft] = useSyncState(0, (next, prev) => {
    if (tabPositionTopOrBottom && onTabScroll) {
      onTabScroll({
        direction: next > prev ? 'left' : 'right',
      });
    }
  });
  const [transformTop, setTransformTop] = useSyncState(0, (next, prev) => {
    if (!tabPositionTopOrBottom && onTabScroll) {
      onTabScroll({
        direction: next > prev ? 'top' : 'bottom',
      });
    }
  });
  const [wrapperScrollWidth, setWrapperScrollWidth] = useState(0);

  const [wrapperScrollHeight, setWrapperScrollHeight] = useState(0);

  const [wrapperContentWidth, setWrapperContentWidth] = useState(0);

  const [wrapperContentHeight, setWrapperContentHeight] = useState(0);

  const [wrapperWidth, setWrapperWidth] = useState(0);

  const [wrapperHeight, setWrapperHeight] = useState(0);

  const [addWidth, setAddWidth] = useState(0);

  const [addHeight, setAddHeight] = useState(0);

  const [tabSizes, setTabSizes] = useState(new Map());

  const tabOffsets = useOffsets(tabs, tabSizes, wrapperScrollWidth);
  const operationsHiddenClassName = ''.concat(
    prefixCls,
    '-nav-operations-hidden',
  );
  let transformMin = 0;
  let transformMax = 0;

  if (!tabPositionTopOrBottom) {
    transformMin = Math.min(0, wrapperHeight - wrapperScrollHeight);
    transformMax = 0;
  } else if (rtl) {
    transformMin = 0;
    transformMax = Math.max(0, wrapperScrollWidth - wrapperWidth);
  } else {
    transformMin = Math.min(0, wrapperWidth - wrapperScrollWidth);
    transformMax = 0;
  }

  const alignInRange = (value: number): [number, boolean] => {
    if (value < transformMin) {
      return [transformMin, false];
    }

    if (value > transformMax) {
      return [transformMax, false];
    }

    return [value, true];
  };

  const touchMovingRef = useRef<number>();
  const [lockAnimation, setLockAnimation] = useState<number | undefined>();
  const doLockAnimation = () => {
    setLockAnimation(Date.now);
  };

  const clearTouchMoving = () => {
    window.clearTimeout(touchMovingRef.current);
  };

  useTouchMove(
    tabsWrapperRef as RefObject<HTMLDivElement>,
    (offsetX: number, offsetY: number) => {
      let preventDefault: boolean = false;

      const doMove: (
        setState: (updater: (per: number) => number) => void,
        offset: number,
      ) => any = (setState, offset) => {
        setState((value: number) => {
          const [newValue, needPrevent] = alignInRange(value + offset);
          preventDefault = needPrevent;
          return newValue;
        });
      };
      if (tabPositionTopOrBottom) {
        if (wrapperWidth >= wrapperScrollWidth) {
          return preventDefault;
        }
        doMove(setTransformLeft, offsetX);
      } else {
        if (wrapperHeight >= wrapperScrollHeight) {
          return preventDefault;
        }

        doMove(setTransformTop, offsetY);
      }
      clearTouchMoving();
      doLockAnimation();
      return preventDefault;
    },
  );

  useEffect(() => {
    clearTouchMoving();
    if (lockAnimation) {
      touchMovingRef.current = window.setTimeout(() => {
        setLockAnimation(0);
      }, 100);
    }
    return clearTouchMoving;
  }, [lockAnimation]);

  const scrollToTab = (...args: any[]) => {
    let key = args.length > 0 && args[0] !== undefined ? args[0] : activeKey;
    let tabOffset = tabOffsets.get(key);
    if (!tabOffset) return;
    if (tabPositionTopOrBottom) {
      let newTransform = transformLeft;
      if (rtl) {
        if (tabOffset.right < transformLeft) {
          newTransform = tabOffset.right;
        } else if (
          tabOffset.right + tabOffset.width >
          transformLeft + wrapperWidth
        ) {
          newTransform = tabOffset.right + tabOffset.width - wrapperWidth;
        }
      } // LTR
      else if (tabOffset.left < -transformLeft) {
        newTransform = -tabOffset.left;
      } else if (
        tabOffset.left + tabOffset.width >
        -transformLeft + wrapperWidth
      ) {
        newTransform = -(tabOffset.left + tabOffset.width - wrapperWidth);
      }
      setTransformTop(0);
      setTransformLeft(alignInRange(newTransform)[0]);
    } else {
      let _newTransform = transformTop;
      if (tabOffset.top < -transformTop) {
        _newTransform = -tabOffset.top;
      } else if (
        tabOffset.top + tabOffset.height >
        -transformTop + wrapperHeight
      ) {
        _newTransform = -(tabOffset.top + tabOffset.height - wrapperHeight);
      }

      setTransformLeft(0);
      setTransformTop(alignInRange(_newTransform)[0]);
    }
  };

  const [visibleStart, visibleEnd] = useVisibleRange(
    tabOffsets,
    {
      width: wrapperWidth,
      height: wrapperHeight,
      left: transformLeft,
      top: transformTop,
    },
    {
      width: wrapperContentWidth,
      height: wrapperContentHeight,
    },
    {
      width: addWidth,
      height: addHeight,
    },
    { ...props, tabs: tabs },
  );

  const tabNodes = tabs.map((tab, index) => {
    let key = tab.key;
    const props = {
      id: id,
      prefixCls: prefixCls,
      key: key,
      rtl: rtl,
      tab: tab,
      closable: tab.closable,
      editable: editable,
      active: key === activeKey,
      tabPosition: tabPosition,
      tabBarGutter: tabBarGutter,
      renderWrapper: children,
      removeAriaLabel:
        locale === null || locale === void 0 ? void 0 : locale.removeAriaLabel,
      onClick: (e: React.MouseEvent | React.KeyboardEvent) => {
        onTabClick(key, e);
      },
      onRemove: () => {
        removeBtnRef(key);
      },
      onFocus: () => {
        scrollToTab(key);
        doLockAnimation(); // Focus element will make scrollLeft change which we should reset back
      },
    };
    return (
      <Draggable key={key} draggableId={key} index={index}>
        {(
          dragProvided: DraggableProvided,
          dragSnapshot: DraggableStateSnapshot,
        ) => (
          <TabNode
            {...props}
            prefixCls="ibr"
            ref={ref => {
              getBtnRef(key).current = ref;
              dragProvided.innerRef(ref);
            }}
            provided={dragProvided}
          ></TabNode>
        )}
      </Draggable>
    );
  });

  const onListHolderResize = useRaf(() => {
    // Update wrapper records
    const offsetWidth = tabsWrapperRef.current?.offsetWidth || 0;
    const offsetHeight = tabsWrapperRef.current?.offsetWidth || 0;
    const newAddWidth = innerAddButtonRef.current?.offsetWidth || 0;
    const newAddHeight = innerAddButtonRef.current?.offsetHeight || 0;
    const newOperationWidth = operationsRef.current?.offsetWidth || 0;
    const newOperationHeight = operationsRef.current?.offsetWidth || 0;

    setWrapperWidth(offsetWidth);
    setWrapperHeight(offsetHeight);
    setAddWidth(newAddWidth);
    setAddHeight(newAddHeight);
    const newWrapperScrollWidth =
      (tabListRef.current?.offsetWidth || 0) - newAddWidth;
    const newWrapperScrollHeight =
      (tabListRef.current?.offsetHeight || 0) - newAddHeight;

    setWrapperScrollWidth(newWrapperScrollWidth);
    setWrapperScrollHeight(newWrapperScrollHeight);
    const isOperationHidden = operationsRef.current?.className.includes(
      operationsHiddenClassName,
    );
    setWrapperContentWidth(
      newWrapperScrollWidth - (isOperationHidden ? 0 : newOperationWidth),
    );
    setWrapperContentHeight(
      newWrapperScrollHeight - (isOperationHidden ? 0 : newOperationHeight),
    ); // Update buttons records

    setTabSizes(() => {
      let newSizes = new Map();
      tabs.forEach(_ref2 => {
        const key = _ref2.key;
        const btnNode = getBtnRef(key).current;
        if (btnNode) {
          newSizes.set(key, {
            width: btnNode.offsetWidth,
            height: btnNode.offsetHeight,
            left: btnNode.offsetLeft,
            top: btnNode.offsetTop,
          });
        }
      });
      return newSizes;
    });
  });

  const startHiddenTabs = tabs.slice(0, visibleStart);
  const endHiddenTabs = tabs.slice(visibleEnd + 1);
  const hiddenTabs = [...startHiddenTabs, ...endHiddenTabs]; // =================== Link & Operations ===================
  let activeTabOffset = tabOffsets.get(activeKey); // Delay set ink style to avoid remove tab blink

  // const [inkStyle, setInkStyle] = useState<TabOffset>();
  // let inkBarRafRef = useRef<number>(0);
  //
  // function cleanInkBarRaf() {
  //   raf.cancel(inkBarRafRef.current);
  // }
  //
  // useEffect(() => {
  //   let newInkStyle: TabOffset = {right: 0, left: 0, width: 0, top: 0, height: 0};
  //
  //   if (activeTabOffset) {
  //     if (tabPositionTopOrBottom) {
  //       if (rtl) {
  //         newInkStyle.right = activeTabOffset.right;
  //       } else {
  //         newInkStyle.left = activeTabOffset.left;
  //       }
  //
  //       newInkStyle.width = activeTabOffset.width;
  //     } else {
  //       newInkStyle.top = activeTabOffset.top;
  //       newInkStyle.height = activeTabOffset.height;
  //     }
  //   }
  //
  //   cleanInkBarRaf();
  //   inkBarRafRef.current = raf(() => {
  //     setInkStyle(newInkStyle);
  //   });
  //   return cleanInkBarRaf;
  // }, [activeTabOffset, tabPositionTopOrBottom, rtl]); // ========================= Effect ========================

  useEffect(() => {
    scrollToTab();
  }, [activeKey, activeTabOffset, tabOffsets, tabPositionTopOrBottom]); // Should recalculate when rtl changed

  useEffect(
    function() {
      onListHolderResize();
    },
    [
      rtl,
      tabBarGutter,
      activeKey,
      tabs
        .map(function(tab) {
          return tab.key;
        })
        .join('_'),
    ],
  ); // ========================= Render ========================

  const hasDropdown = !!hiddenTabs.length;
  const wrapPrefix = ''.concat(prefixCls, '-nav-wrap');
  let pingLeft;
  let pingRight;
  let pingTop;
  let pingBottom;

  if (tabPositionTopOrBottom) {
    if (rtl) {
      pingRight = transformLeft > 0;
      pingLeft = transformLeft + wrapperWidth < wrapperScrollWidth;
    } else {
      pingLeft = transformLeft < 0;
      pingRight = -transformLeft + wrapperWidth < wrapperScrollWidth;
    }
  } else {
    pingTop = transformTop < 0;
    pingBottom = -transformTop + wrapperHeight < wrapperScrollHeight;
  }
  /* eslint-disable jsx-a11y/interactive-supports-focus */

  const onDragEnd = (result: DropResult) => {
    // super simple, just removing the dragging item
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    if (result.destination.index === result.source.index) {
      return;
    }

    const reorder = (
      list: Tab[],
      startIndex: number,
      endIndex: number,
    ): Tab[] => {
      const [removed] = list.splice(startIndex, 1);
      list.splice(endIndex, 0, removed);
      return list;
    };

    tabs = reorder(tabs, result.source.index, result.destination.index);
  };

  return (
    <div
      ref={ref}
      role="tablist"
      className={classNames(''.concat(prefixCls, '-nav'), className)}
      style={style}
      onKeyDown={() => {
        doLockAnimation();
      }}
    >
      <ExtraContent position="left" extra={extra} prefixCls={prefixCls} />
      <OperationNode
        ref={operationsRef}
        {...props}
        prefixCls={prefixCls}
        tabs={hiddenTabs}
        className={!hasDropdown ? operationsHiddenClassName : void 0}
      />
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId={id} direction="horizontal">
          {(
            dropProvided: DroppableProvided,
            dropSnapshot: DroppableStateSnapshot,
          ) => (
            <ResizeObserver onResize={onListHolderResize}>
              <div
                ref={ref => {
                  tabsWrapperRef.current = ref;
                  dropProvided.innerRef(ref);
                }}
                className={classNames(wrapPrefix, {
                  [''.concat('ibr', '-scroll-tabs', '-node')]: true,
                  [''.concat(wrapPrefix, '-ping-left')]: pingLeft,
                  [''.concat(wrapPrefix, '-ping-right')]: pingRight,
                  [''.concat(wrapPrefix, '-ping-top')]: pingTop,
                  [''.concat(wrapPrefix, '-ping-bottom')]: pingBottom,
                })}
                {...dropProvided.droppableProps}
              >
                <ResizeObserver onResize={onListHolderResize}>
                  <div
                    ref={tabListRef as LegacyRef<HTMLDivElement>}
                    className={''.concat(prefixCls, '-nav-list')}
                    style={{
                      transform: 'translate('
                        .concat(transformLeft.toString(), 'px, ')
                        .concat(transformTop.toString(), 'px)'),
                      transition: lockAnimation ? 'none' : undefined,
                    }}
                  >
                    {/*<DragDropContext onDragEnd={()=>{}}>*/}
                    {/*                <Droppable droppableId="droppable" direction="horizontal">*/}
                    {/*                  {(provided, snapshot)=>()}*/}
                    {/*                </Droppable>*/}
                    {/*</DragDropContext>*/}
                    {tabNodes}
                  </div>
                </ResizeObserver>
              </div>
            </ResizeObserver>
          )}
        </Droppable>
      </DragDropContext>
      <AddButton
        ref={innerAddButtonRef as RefObject<HTMLButtonElement>}
        prefixCls={prefixCls}
        locale={locale}
        editable={editable}
        style={{
          visibility: !hasDropdown ? 'hidden' : undefined,
        }}
      />
      <ExtraContent position="right" extra={extra} prefixCls={prefixCls} />
    </div>
  );

  /* eslint-enable */
};

const TabNavListNew = React.forwardRef(TabNavList);

export const rederBar = (
  props: TabNavListProps,
  DefaultTabBar: React.ComponentType,
): React.ReactElement => {
  return <TabNavListNew {...props} />;
};
