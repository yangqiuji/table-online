import { connect, Dispatch } from 'umi';
import React from 'react';
import './layout.less';
import TopBar from '@/components/TopBar/TopBar';
import ThemeProvider from '@/components/ThemeSelector/Theme';

const MainLayout: React.FC<BasicLayoutProps> = props => {
  const { children } = props;

  return (
    <ThemeProvider>
      <div className="main-windows">
        <TopBar className="main-top-bar"></TopBar>
        {children}
      </div>
    </ThemeProvider>
  );
};

export interface BasicLayoutProps {
  dispatch: Dispatch;
}

export interface GlobalModelState {
  collapsed: boolean;
}

export default connect(() => ({}))(MainLayout);
