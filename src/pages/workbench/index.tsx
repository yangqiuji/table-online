import React, { useContext } from 'react';
import './index.less';
import {
  Button,
  ButtonGroup,
  Icon,
  InputGroup,
  ITreeNode,
  Tree,
} from '@blueprintjs/core';
import ThemeContext, {
  themeVaule,
} from '@/components/ThemeSelector/ThemeContext';
import { ResizeBox, ResizeLeft } from '@/components/ResizeBox';
import ScrollTabs, { tabPans } from '@/components/ScrollTabs/ScrollTabs';
import TableTitleBar from '@/pages/workbench/TableTitleBar';
import GridView from '@/components/ScrollTabs/views/GridView';

export default () => {
  const { theme }: themeVaule = useContext(ThemeContext);

  const INITIAL_STATE: ITreeNode[] = [
    {
      id: 1,
      hasCaret: true,
      icon: 'folder-close',
      label: '项目交付文档',
      childNodes: [
        {
          id: 2,
          hasCaret: true,
          icon: 'folder-close',
          label: '项目任务协助',
        },
        {
          id: 3,
          hasCaret: true,
          icon: 'folder-close',
          label: '新建文件夹',
        },
      ],
    },
  ];

  const featureLibrary: ITreeNode[] = [
    {
      id: 'library',
      icon: <Icon icon="flag"></Icon>,
      label: <p style={{ color: theme.colorTextTopTitle }}>library feature</p>,
      hasCaret: false,
      isExpanded: true,
      childNodes: [
        {
          id: 'trash',
          icon: <Icon icon="trash"></Icon>,
          label: <p style={{ color: '#CED9E0' }}>回收站</p>,
        },
        {
          id: 'favorite',
          icon: <Icon icon="favorite"></Icon>,
          label: <p style={{ color: '#CED9E0' }}>收藏夹</p>,
        },
        {
          id: 'recents',
          icon: <Icon icon="history"></Icon>,
          label: <p style={{ color: '#CED9E0' }}>最近使用</p>,
        },
      ],
    },
  ];

  const searchIconElemet = (
    <Icon icon="search" color={theme.colorTextSearch}></Icon>
  );
  const settingIconElemet = (
    <Icon icon="settings" color={theme.colorTextSearch}></Icon>
  );
  const settingButton = (
    <Button
      icon={settingIconElemet}
      color={theme.colorTextSearch}
      className="workbench-box-sider-setting-button"
      minimal
    />
  );

  const leftElement = (
    <ResizeLeft>
      <InputGroup
        leftElement={searchIconElemet}
        className="workbench-box-sider-search"
        placeholder="搜索"
        style={{ borderRadius: 0 }}
        rightElement={settingButton}
      ></InputGroup>
      <Tree
        contents={featureLibrary}
        className="workbench-box-sider-features-tree"
      ></Tree>
      <Tree
        contents={INITIAL_STATE}
        className="workbench-box-sider-contents-tree"
      ></Tree>
    </ResizeLeft>
  );

  const extraContent = {
    left: <TableTitleBar />,
    right: (
      <ButtonGroup
        fill={true}
        minimal={true}
        className="ibr-scroll-tabs-extra-right"
      >
        <Button icon="database" />
        <Button icon="function" />
      </ButtonGroup>
    ),
  };

  const panes: tabPans[] = [
    {
      key: '1',
      value: '表格视图',
      paneContent: <GridView />,
      paneProps: { className: 'ibr-scroll-tabs-tab-pane' },
    },
    {
      key: '2',
      value: '相册视图',
      paneContent: <div>相册视图</div>,
      paneProps: {},
    },
    {
      key: '3',
      value: '日历视图',
      paneContent: <div>日历视图</div>,
      paneProps: {},
    },
    {
      key: '4',
      value: '看板视图',
      paneContent: <div>看板视图</div>,
      paneProps: {},
    },
  ];

  const rightElement = (
    <ScrollTabs tabBarExtraContent={extraContent} panes={panes} />

    // <div className='ibr-content-main-content'>
    // </div>);
  );
  return (
    <ResizeBox
      titleDisplay={true}
      leftElement={leftElement}
      rightElement={rightElement}
      className="ibr-content-container"
    />
  );
};
