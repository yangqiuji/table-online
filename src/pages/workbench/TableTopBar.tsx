import Props from '@/common/props';
import React, { useContext } from 'react';
import ThemeContext from '@/components/ThemeSelector/ThemeContext';
import { Icon, Label, Popover } from '@blueprintjs/core';
import classNames from 'classnames';
import PopmenuOnTableTitle from '@/pages/workbench/PopmenuOnTableTitle';
import ScrollTabs from '@/components/ScrollTabs/ScrollTabs';
import { Tabs } from 'antd';

const { TabPane } = Tabs;

interface TableTopBarProps extends Props {}

const TableTopBar: React.FC<TableTopBarProps> = props => {
  const { className } = props;
  const { theme } = useContext(ThemeContext);
  const topBarClasses = classNames(
    'ibr-content-table-top-bar-title',
    className,
  );

  const callback = key => {
    console.log(key);
  };

  return (
    <div className={topBarClasses}>
      <div>
        <Label>个人账单信息超级长的标题</Label>
        <Icon
          icon="info-sign"
          iconSize={10}
          className="ibr-content-table-top-bar-title-info"
        ></Icon>
        <Popover>
          <Icon
            icon="caret-down"
            iconSize={22}
            className="ibr-content-table-top-bar-title-menu"
          ></Icon>
          <PopmenuOnTableTitle />
        </Popover>
      </div>
      <ScrollTabs
        className="ibr-content-table-top-bar-tabs"
        tapsId="table-views"
      />

      <div className="ibr-content-table-top-bar-tools"></div>
    </div>
  );
};

export default TableTopBar;
