import Props from '@/common/props';
import React from 'react';
import { Menu } from '@blueprintjs/core';

const PopmenuOnTableTitle: React.FC<Props> = props => {
  return (
    <Menu>
      <Menu.Item icon="annotation" text="重命名"></Menu.Item>
      <Menu.Item icon="info-sign" text="编辑描述信息"></Menu.Item>
      <Menu.Item icon="lock" text="设置权限"></Menu.Item>
      <Menu.Item icon="duplicate" text="复制拷贝"></Menu.Item>
      <Menu.Item icon="trash" text="删除"></Menu.Item>
    </Menu>
  );
};

export default PopmenuOnTableTitle;
