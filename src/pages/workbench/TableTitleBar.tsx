import React, { useContext } from 'react';
import Props from '@/common/props';
import { Icon, Label } from '@blueprintjs/core';
import PopmenuOnTableTitle from '@/pages/workbench/PopmenuOnTableTitle';
import ThemeContext from '@/components/ThemeSelector/ThemeContext';
import classNames from 'classnames';
import { Popover2 } from '@blueprintjs/popover2';

const TableTitleBar: React.FC<Props> = (props: Props) => {
  const themes = useContext(ThemeContext);
  const iconS = 'map';
  const { className } = props;

  return (
    <div className={classNames('ibr-scroll-tabs-extra-left', className)}>
      <Popover2>
        <div className="ibr-scroll-tabs-extra-left-title">
          <Icon icon={iconS} style={{ margin: '5px 4px 0 0' }} />w
          <Label>个人账单信息超长信息</Label>
          <Icon icon="caret-down" iconSize={14}></Icon>
        </div>
        <PopmenuOnTableTitle />
      </Popover2>
      <Icon
        icon="info-sign"
        iconSize={12}
        className="ibr-scroll-tabs-extra-left-button-info"
      ></Icon>
    </div>
  );
};

export default TableTitleBar;
