import Props from '@/common/props';
import React, { useContext } from 'react';
import ThemeContext from '@/components/ThemeSelector/ThemeContext';
import { Button, Icon, Label, Tab, Tabs } from '@blueprintjs/core';

interface TableConsoleProps extends Props {}

const TableConsole: React.FC<TableConsoleProps> = props => {
  const { theme } = useContext(ThemeContext);

  return (
    <div className="ibr-content-table-console">
      <div className="ibr-content-table-console-title">
        <Label>个人账单信息超级长的标题</Label>
        <Icon icon="error" iconSize={12}></Icon>
        <Icon icon="caret-down" iconSize={20}></Icon>
      </div>
      <Tabs id="datas-views" className="ibr-content-table-console-tabs">
        <Button outlined={false} icon="menu"></Button>
        <Tab id="grid" title="grid视图" />
        <Tab id="gallery" title="相册视图" />
        <Tab id="kanban" title="看板视图" />
        <Tab id="from" title="表单视图" />
        <Tab id="calendar" title="日历视图" />
      </Tabs>
      <div
        id="table_tool_bar"
        className="ibr-content-table-top-bar-tools"
      ></div>
      <div
        id="table_details"
        className="ibr-content-table-console-tables"
      ></div>
    </div>
  );
};

export default TableConsole;
