export default [
  {
    path: '/',
    component: '@/layouts/MainLayout',
    routes: [
      {
        path: '/workbench',
        name: 'workbench',
        icon: 'smile',
        component: './workbench/index',
      },
      {
        path: '/workspace',
        name: 'workspace',
        icon: 'smile',
        component: './workspace/index',
      },
    ],
  },
];
