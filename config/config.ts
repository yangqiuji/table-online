import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  routes,
  fastRefresh: {},
  hash: true,
  dva: {
    hmr: true,
  },
  history: {
    type: 'browser',
  },
});
